# Ansible Role {{cookiecutter.role_name}}

{{cookiecutter.description}}

## Usage

Add this role to your ``requirements.yml``

```yaml
- name: {{cookiecutter.role_name}}
  src: git+ssh://{{ cookiecutter.project_git.ssh }}
  version: 0.0.1.dev
```

### Using in Playbook

```yaml
- hosts: rocketchat
  become: true
  tasks:
    - include_role:
        name: {{cookiecutter.role_name}}
```


## Development

For Development you need some dependencies listed in the ``requirements.txt``. It is Recommendet to use a seperate ``virtualenv``.

```bash
{% if cookiecutter.molecule_driver == "docker" %}
virtualenv -p python3 ~/venvs/develop-ansible_role-docker
source ~/venvs/develop-ansible_role-docker/bin/activate
{% endif %}
{% if cookiecutter.molecule_driver == "vagrant" %}
virtualenv -p python3 ~/venvs/develop-ansible_role-vagrant
source ~/venvs/develop-ansible_role-vagrant/bin/activate
{% endif %}
pip install -r requirements.txt
```

## Testing

```bash
molecule test
```

### Releasing

Must be executed from the ``develop`` branch.

```bash
pre-commit uninstall \
    && bumpversion --tag release \
    && git checkout master && git merge develop && git checkout develop \
    && bumpversion --no-tag patch \
    && git push origin master --tags \
    && git push origin develop \
    && pre-commit install
```
