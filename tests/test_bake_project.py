import filecmp
import os
import pytest

__location__ = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__)))

def test_bake_project(cookies):
    result = cookies.bake(extra_context={'role_name': 'role_some_test'})

    assert result.exit_code == 0
    assert result.exception is None
    assert result.project.basename == 'role_some_test'
    assert result.project.isdir()

@pytest.mark.parametrize(
    "cookiecutter_extra_context,expected_molecule_golden_file",
    [
        ({'molecule_driver': 'vagrant'},"golden_files/molecule_vagrant.yml"),
        ({},"golden_files/molecule_docker.yml"),
    ],
)
def test_create_vagrand_based(cookies,cookiecutter_extra_context, expected_molecule_golden_file):
    result = cookies.bake(extra_context=cookiecutter_extra_context)
    molecule_conf_file = result.project.join('molecule/default/molecule.yml')
    golden_file = open(os.path.join(__location__, expected_molecule_golden_file ))
    molecule_conf_lines = molecule_conf_file.readlines(cr=True)
    golden_file_lines = golden_file.readlines()
    assert molecule_conf_lines == golden_file_lines
