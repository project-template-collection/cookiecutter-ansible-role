# Template for new Ansible Roles


## Usage

```bash
virtualenv -p python3 ~/venvs/usage-cookiecutter
source ~/venvs/usage-cookiecutter/bin/activate
pip install cookiecutte>=1.6.0
```

```bash
cookiecutter https://gitlab.com/project-template-collection/cookiecutter-ansible-role.git --checkout v0.0.8.dev
```

## Development

For Development you need some dependencies listed in the ``requirements.txt``.

```bash
virtualenv -p python3 ~/venvs/develop-cookiecutter
source ~/venvs/develop-cookiecutter/bin/activate
pip install -r requirements.txt
```

### Testing

```bash
py.test -v tests/*
```

### Releasing

Must be executed from the ``develop`` branch.

```bash
pre-commit uninstall \
    && bumpversion --tag release \
    && git checkout master && git merge develop && git checkout develop \
    && bumpversion --no-tag patch \
    && git push origin master --tags \
    && git push origin develop \
    && pre-commit install
```
